1-select * from departments;
2-select UPPER(first_name||' '||last_name) as "nom et prenom" from employees WHERE last_name LIKE 'A%';
3-select DEPARTMENT_ID as "numero du departement",last_name,first_name,salary from employees ORDER BY DEPARTMENT_ID,salary DESC;
-- didn't quite understand the question (semestre,trimestre).
4-select extract(year FROM hire_date) as "année",extract(month FROM hire_date) as "mois" FROM employees ORDER BY "année" DESC;
-- didn't quite understand the question (semestre,trimestre).
5-select department_id,sum(extract(month from hire_date)) as "somme des mois" FROM employees GROUP BY department_id;
-- probably didn't understand the question correctly.
6-select department_id "id depart",sum(salary) "salaires" FROM employees GROUP BY department_id;
7-select count(*) "l'an 2000" FROM employees where extract(year from hire_date) = 2000;
8-select location_id "id location",count(department_id) as "nombres des departements" from departments GROUP BY location_id having count(department_id) > 2;
-- not sure

-- 9- i have no idea what is demmanded

10- methode 1: select employees.*,jobs.job_title "travaille" from employees JOIN jobs ON employees.job_id=jobs.job_id;
methode 2 : select employees.*,(select job_title from jobs where employees.job_id=jobs.job_id) "travaille" from employees;

11- select d.department_name,c.country_name,r.region_name FROM departments d 
JOIN locations l ON l.location_id=d.location_id 
JOIN countries c ON c.country_id=l.country_id 
JOIN regions r ON r.region_id=c.region_id ORDER BY d.department_id;

12- select * from employees where manager_id=(select manager_id from employees where last_name='Jones');

13- select * from employees where salary=(select min(salary) from employees);

14- select first_name,case when extract(year from hire_date)=1998 then 'NEEDS REVIEW' else 'NOT THIS YEAR' end as condition from employees;

15 -  select d.department_name,(select max(salary) from employees e 
	where d.department_id=e.department_id) "maximum",(select min(salary) from employees e where (d.department_id=e.department_id or d.department_id=null)) "minimum" 
	from departments d ORDER BY "minimum";
-- doesn't show me what i need why?
16- select * from employees where salary > (select max(salary) from employees where job_id='SA_MAN');
17-select * from employees l where salary<(select avg(salary) from employees where l.department_id=employees.department_id);