CREATE TABLE Vehicules(
	immatriculation varchar2(30) not null  CONSTRAINT immatriculation_primarykey PRIMARY KEY,
	Ref_model varchar2(30) not null,
	Date_mise_circulation DATE not null,
	Kilometrage number not null,
	Prix_demande number not null,
	CONSTRAINT ref_model_foreignkey FOREIGN KEY(Ref_model) REFERENCES models(Ref_model)
);

CREATE TABLE Models(
	Ref_model varchar2(30) not null CONSTRAINT ref_model_primarykey PRIMARY KEY,
	Modele varchar2(30) not null,
	Marque varchar2(30) not null,
	Puissance varchar2(30) not null
);

CREATE TABLE Personnes(
	CIN varchar2(15) not null CONSTRAINT cin_primarykey PRIMARY KEY,
	Nom varchar2(30) not null,
	Prenom varchar2(30) not null,
	Email varchar2(30) not null UNIQUE,
	CONSTRAINT email_param CHECK (Email LIKE '%@.%') 
);


CREATE TABLE Acquisitions(
	immatriculation varchar2(30) not null  CONSTRAINT immatriculation_foreignkey references Vehicules(immatriculation),
	Cin not null  CONSTRAINT cin_foreignkey references Personnes(CIN),
	Date_debut date,
	Date_fin date not null,
	CONSTRAINT Acquisitions_primarykey PRIMARY KEY(immatriculation,Cin),
	CONSTRAINT date_fin_constraint check (Date_fin > Date_debut)
);

CREATE TABLE Contrat_vente(
	Ref_contrat varchar2(30),
	CIN varchar2(30) references Personnes(CIN),
	immatriculation varchar2(30) references Vehicules(immatriculation),
	Date_vente timestamp,
	Prix_vente numeric not null,
	CONSTRAINT COntrat_vente PRIMARY KEY (Ref_contrat,CIN,immatriculation)
);

ALTER TABLE Models ADD CONSTRAINT models_check check (Puissance='4CH' OR Puissance='5CH' OR Puissance='6CH');
ALTER TABLE Vehicules ADD CONSTRAINT vehicules_check check (Prix_demande between 1000 AND 100000);
ALTER TABLE Personnes ADD CONSTRAINT personnes_check check (Nom <> Prenom);
ALTER TABLE Acquisitions MODIFY Date_debut date not null;