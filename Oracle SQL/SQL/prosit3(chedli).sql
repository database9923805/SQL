-- SELECT table_name FROM user_tables;
INSERT INTO modelees values('RC','Clio','Renault','5CH');
INSERT INTO modelees values('PCC','206cc','Peugeot','5CH');
INSERT INTO modelees values('FPE','Punto Evo','Fiat','4CH');
INSERT INTO modelees values('FF','Fiesta','Ford','6CH');

INSERT INTO vehiculees values ('1245Tunis99','RC',to_date('14/03/2001','DD/MM/YYYY'),155000,10200);
INSERT INTO vehiculees values ('264Tunis142','FF',to_date('11/02/2010','DD/MM/YYYY'),75000,19500);
INSERT INTO vehiculees values ('569Tunis122','PCC',to_date('02/11/2005','DD/MM/YYYY'),85250,15200);
INSERT INTO vehiculees values ('1713Tunis154','FPE',to_date('10/11/2012','DD/MM/YYYY'),106000,21000);

INSERT INTO Personnees values ('03645148','Masmoudi','Ahmed','Ahmed.masmoudi@gmail.com','70983123');
INSERT INTO Personnees(CIN,NOM,PRENOM,EMAIL) values ('07498662','Ayadi','Omar','Omar.ayadi@gmail.com');
INSERT INTO Personnees(CIN,NOM,PRENOM,EMAIL) values ('07895624','Ben Chaabane','Mariem','Mariem.benchaabane@yahoo.fr');
INSERT INTO Personnees values ('06784512','Bali','Imen','Imen.bali@gmail.com','72145870');
INSERT INTO Personnees values ('04456641','Salhi','Ali','Ali.Salhi@outlook.fr','70983145');
-- Madame vous avez un probleme avec le REGEX ce n'est pas ( REGEXP_LIKE(num_tel,'^[2,5,9][0-9]{7}$')) Mais '^[0-9]{8}$' pour la contraint de num_tel_valide de la table personnees

INSERT INTO ACQUISITIONNS values ('1245Tunis99','03645148',to_date('20/06/2007','DD/MM/YYYY'),to_date('17/11/2010','DD/MM/YYYY'));
INSERT INTO ACQUISITIONNS values ('264Tunis142','07895624',to_date('11/01/2012','DD/MM/YYYY'),to_date('23/10/2020','DD/MM/YYYY'));
INSERT INTO ACQUISITIONNS values ('1713Tunis154','04456641',to_date('03/01/2014','DD/MM/YYYY'),to_date('02/01/2015','DD/MM/YYYY'));
INSERT INTO ACQUISITIONNS values ('1245Tunis99','07498662',to_date('18/11/2010','DD/MM/YYYY'),to_date('15/06/2016','DD/MM/YYYY'));

INSERT INTO contrat_ventee values ('C104','03645148','1245Tunis99',to_date('20/06/2007','DD/MM/YYYY'),9600);
INSERT INTO contrat_ventee values ('C105','07895624','264Tunis142',to_date('','DD/MM/YYYY'),19000);
INSERT INTO contrat_ventee values ('C106','04456641','1713Tunis154',to_date('','DD/MM/YYYY'),20800);
INSERT INTO contrat_ventee values ('C107','07498662','1245Tunis99',to_date('18/11/2010','DD/MM/YYYY'),8500);

ALTER TABLE vehiculees ADD (COULEUR varchar2(20));
UPDATE vehiculees SET COULEUR='BLEU' WHERE IMMATRICULE='1713Tunis154';
UPDATE (
	SELECT vehiculees.prix_demande,vehiculees.immatricule FROM vehiculees
	INNER JOIN acquisitionns ON acquisitionns.immatriculation = vehiculees.immatricule
	INNER JOIN personnees ON acquisitionns.cin = personnees.cin
	WHERE acquisitionns.cin = '1245Tunis99' AND personnees.nom = 'Masmoudi'
)
SET vehiculees.prix_demande = vehiculees.prix_demande-300;
DELETE FROM contrat_ventee WHERE date_vente > to_date('01/01/2008','DD/MM/YYYY');

