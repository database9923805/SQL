set serveroutput on;

PARTIE 2:
1)
-- type tablemul IS record ( nom varchar(30), nombre number);
-- emp tablemul;
-- select department_name,(select count(*) from employees where department_id = d.department_id) as c
-- into emp.nom,emp.nombre from departments d order by c desc;
a--
-- Afficher la liste des départements, leur identifiant, leur nom ainsi leur salaire moyen
--i En utilisant un curseur implicite

BEGIN
	FOR emp IN (select department_id,department_name,(select avg(salary) from employees where d.department_id =  department_id) as moyenne from departments d) LOOP
		dbms_output.put_line('department name : '||emp.department_id|| ' nom du départment : ' || emp.department_name || ' moyenne du salaire : ' || emp.moyenne);  
	END LOOP;
END;
/
--ii En utilisant un curseur explicite
DECLARE
	CURSOR affichage IS
	 select department_id,department_name,(select avg(salary) from employees where d.department_id =  department_id) as moyenne from departments d;
BEGIN
	FOR emp IN affichage LOOP
		dbms_output.put_line('department name : '||emp.department_id|| ' nom du départment : ' || emp.department_name || ' moyenne du salaire : ' || emp.moyenne);  
	END LOOP;
END;
/
-- select department_name,(select count(*) from employees where department_id = d.department_id) as c  from departments d order by c desc;
b--
DECLARE
	CURSOR affichage
	  IS
	 select department_name as b,(select count(*) from employees where department_id = d.department_id) as c from departments d order by c desc;
BEGIN
	FOR emp IN affichage LOOP
		dbms_output.put_line('department name : '||emp.b|| ' nombres des employees : ' || to_char(emp.c));  
	END LOOP;
END;
/

c--	
DECLARE
	CURSOR affichage
	  IS
	 select department_name,first_name from departments join employees ON employees.department_id = departments.department_id where (select count(*) from employees where department_id = departments.department_id) > 20 order by departments.department_name;
BEGIN 
	FOR emp IN affichage LOOP
		dbms_output.put_line('department name : '||emp.department_name || ' nom : ' || to_char(emp.first_name));  
	END LOOP;
END;
/

d--
DECLARE
	CURSOR affichage IS
	 select employee_id,first_name,last_name from employees join jobs on jobs.job_id = employees.job_id where jobs.job_title like '%Manager';
	 CURSOR employee(id number) IS 
	 select first_name,last_name from employees where manager_id = id;

BEGIN 
	FOR emp IN affichage LOOP
	   dbms_output.put_line('Manager name : '||emp.first_name || '    last name : ' || emp.last_name);  
	    FOR worker IN employee(emp.employee_id) LOOP
			dbms_output.put_line('----------employee name : '||worker.first_name || '    last name : ' || worker.last_name);  
		END LOOP;
	END LOOP;
END;
/

2--no idea how i need to do this with cursors
BEGIN 
	CREATE TABLE PRODUIT(
	 	idProduit int PRIMARY KEY NOT NULL,
	 	nomProduit varchar(30),
	 	categorieP varchar(30),
	 	prixProduit numeric
	 );
END;
/