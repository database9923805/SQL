Declare
vsql1 varchar2(200) :='Create table OUVRIERS(Matricule number primary key,
Nom varchar(20),
prenom varchar(20),
PrixJr number,
Ref number references chantiers(reference))';
vsql2 varchar(200) :='create table chantiers(
Reference number primary key,
Lieu varchar(20),
dateDebut date, Duree integer,
NbrOuvrierAffectes number)';
BEGIN
Execute immediate(vsql2);
Execute immediate(vsql1);
END;
/

create or replace procedure proc_insertion_CHANTIER(ref chantiers.reference%type,
lieu chantiers.lieu%type,
datedebut chantiers.datedebut%type,
duree chantiers.duree%type) is
BEGIN
insert into chantiers values(Ref, Lieu, DateDebut, Duree, null);
EXCEPTION
when dup_val_on_index then
dbms_output.put_line('error');
END;
/


begin
proc_insertion_CHANTIER(1, 'lieu1', sysdate, 12);
end;
/

create or replace procedure proc_insertion_OUVRIER(Mat ouvriers.Matricule%type,
Nom ouvriers.nom%type,
Prenom ouvriers.prenom%type,
PrixJr ouvriers.prixjr%type,
ref ouvriers.ref%type) is
b exception;
begin
if prixjr<10 then
raise b;
end if;
insert into ouvriers values(mat, nom, prenom, prixjr, ref);
exception
when b then
dbms_output.put_line('le prix est inferieur a 10');
when no_data_found then
dbms_output.put_line('chantier nexiste pas');
when dup_val_on_index then
dbms_output.put_line('error');
end;
/

begin
proc_insertion_OUVRIER(3, 'o1', 'p1', 12, 2);
end;
/