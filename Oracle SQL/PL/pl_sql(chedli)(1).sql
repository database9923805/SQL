set serveroutput on;
1)
a--
BEGIN
  dbms_output.put_line(sysdate);
END;
/
b--
DECLARE
 s1 numeric(36,2) := 44.5;
 s2 numeric(36,2) :=  61.8;
BEGIN
  dbms_output.put_line(s1+s2);
END;
/
c--
DECLARE
s number := 102;
BEGIN
FOR v1 IN 1..10 LOOP
	dbms_output.put(v1||',');
END LOOP;
dbms_output.put_line('');
WHILE s > 0 LOOP
	s:=s-2;
	dbms_output.put(s||',');
END LOOP;
dbms_output.put_line('');
END;
/
d--
DECLARE
 s1 number:=5;
 s2 number:=3;

BEGIN
  dbms_output.put_line('somme : '||to_char(s1+s2));
  dbms_output.put_line('produit : '||to_char(s1*s2));
END;
/
e--
DECLARE
	type grades IS VARRAY(3) OF INTEGER;
	s1 grades:= grades(5,2,3);
	s_sorted grades;
BEGIN
 s1.extend;
 FOR i in s1.first .. s1.last LOOP
 	dbms_output.put_line(s1(i));
 END LOOP;
END;
/

-- blocked
f--
DECLARE
	s number := 5;
	fac number := 1;
BEGIN
while s > 0 loop  
	fac:=s*fac;        
	s:=s-1;           
end loop;
dbms_output.put_line('le factoriel est : '||to_char(fac));  
END;
/

2)
a--
DECLARE
	nom varchar(50) := 5;
BEGIN
	select first_name into nom from employees where employee_id = 110; 
dbms_output.put_line('le nom est : '||nom);  
END;
/
b--
DECLARE
	counter number;
BEGIN
	select count(*) into counter from employees where department_id = 50;
	dbms_output.put_line('nombre des employees dans le departement est : '||counter);  
END;
/
c--
DECLARE
	mini number;
	maxi number;
BEGIN
	select min(salary),max(salary) into mini,maxi from employees where department_id = 50;
	dbms_output.put_line('max est : '||to_char(maxi)|| ' min est : ' || to_char(mini));  
END;
/
