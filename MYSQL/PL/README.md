# plsql

## Getting Started

You need to login using : 

```
mysql -u username -p'your_password'
```

## Command

To execute the file you can either : 

```
mysql -u username -p'your_password' database < 'drag your file here'
OR inside the mysql command line 
- use database;
- source 'drag your file here' and remove ' '
```