-- remove all tables in a database except NOT IN tables
BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE dropStmt VARCHAR(255);
  DECLARE cur CURSOR FOR
    SELECT CONCAT('DROP TABLE IF EXISTS `', table_name, '`;')
    FROM information_schema.tables
    WHERE table_schema = 'youtubeAPI'
      AND table_name NOT IN ('google_users', 'google_user_youtube_channel', 'youtube_channels');
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur;

  read_loop: LOOP
    FETCH cur INTO dropStmt;
    IF done THEN
      LEAVE read_loop;
    END IF;
    SET @dropStmt = dropStmt;
    PREPARE dropStmt FROM @dropStmt;
    EXECUTE dropStmt;
    DEALLOCATE PREPARE dropStmt;
  END LOOP;

  CLOSE cur;
END;
